//
//  SelectedCityController.swift
//  TripPlan
//
//  Created by Aung Khant Thet Naing on 1/26/16.
//  Copyright © 2016 Aung Khant Thet Naing. All rights reserved.
//

import UIKit

class SelectedCityController: UIViewController {
    @IBOutlet var bgImgView:UIImageView!
    var blurEffectView:UIVisualEffectView?
    var trips:Attraction!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var bgImg = trips.featuredImage
    //    bgImgView.image = UIImage(named: bgImg)
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView?.frame = view.bounds
    }
}
