//
//  SignUpViewController.swift
//  TripPlan
//
//  Created by Aung Khant Thet Naing on 1/25/16.
//  Copyright © 2016 Aung Khant Thet Naing. All rights reserved.
//

import UIKit

class signUpViewController: UIViewController {
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var emailAddress: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    @IBAction func signUp(sender: AnyObject) {
        
        let alertController = UIAlertController(title: "Agree to terms and conditions",
            message: "Click I AGREE to signal that you agree to the End User Licence Agreement.",
            preferredStyle: UIAlertControllerStyle.Alert
        )
        alertController.addAction(UIAlertAction(title: "I AGREE",
            style: UIAlertActionStyle.Default,
            handler: { alertController in self.processSignUp()})
        )
        alertController.addAction(UIAlertAction(title: "I do NOT agree",
            style: UIAlertActionStyle.Default,
            handler: nil)
        )
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func signIn(sender: AnyObject) {
        
        activityIndicator.hidden = false
        activityIndicator.startAnimating()
        
        let userEmailAddress = emailAddress.text
        let userPassword = password.text
        
        PFUser.logInWithUsernameInBackground(userEmailAddress!, password:userPassword!) {
            (err: PFUser?, error: NSError?) -> Void in
            if err != nil {
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier("selectCity", sender: self)
                }
            } else {
                self.activityIndicator.stopAnimating()
                
                if let message: AnyObject = error!.userInfo["error"] {
                    self.message.text = "\(message)"
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.hidden = true
        activityIndicator.hidesWhenStopped = true
    }
    
    func processSignUp() {
        
        let userEmailAddress = emailAddress.text
        let userPassword = password.text
        
        activityIndicator.hidden = false
        activityIndicator.startAnimating()
        
        let user = PFUser()
        user.username = userEmailAddress
        user.password = userPassword
        user.email = userEmailAddress
        
        user.signUpInBackgroundWithBlock {
            (succeeded: Bool, error: NSError?) -> Void in
            if error == nil {
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.performSegueWithIdentifier("signUp", sender: self)
                }
                
            } else {
                
                self.activityIndicator.stopAnimating()
                
                if let message: AnyObject = error!.userInfo["error"] {
                    self.message.text = "\(message)"
                }
            }
        }
    }
    
    @IBAction func forgottenPassword(sender: AnyObject) {
        
        let targetEmailAddress = emailAddress.text?.lowercaseString
        
        if targetEmailAddress == "" {
            let alertController = UIAlertController(title: "Missing Email Address",
                message: "Please provide an email address so that we can process your password request.",
                preferredStyle: UIAlertControllerStyle.Alert
            )
            alertController.addAction(UIAlertAction(title: "Okay",
                style: UIAlertActionStyle.Default,
                handler: nil)
            )
            self.presentViewController(
                alertController,
                animated: true,
                completion: nil
            )
            
        } else {
            
            PFUser.requestPasswordResetForEmailInBackground(targetEmailAddress!, block: nil)
            
            let alertController = UIAlertController(title: "Password Reset",
                message: "If an account with that email address exists - we have sent an email containing instructions on how to complete your request.",
                preferredStyle: UIAlertControllerStyle.Alert
            )
            alertController.addAction(UIAlertAction(title: "Okay",
                style: UIAlertActionStyle.Default,
                handler: nil)
            )
            self.presentViewController(
                alertController,
                animated: true,
                completion: nil
            )
        }
    }
    
    
    
    
}
