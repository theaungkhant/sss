//
//  CityViewController.swift
//  TripPlan
//
//  Created by Aung Khant Thet Naing on 1/25/16.
//  Copyright © 2016 Aung Khant Thet Naing. All rights reserved.
//

import UIKit

class CityViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var backgroundImageView:UIImageView!
    @IBOutlet var collectionView:UICollectionView!
    
    private var trips = [Attraction]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadTripsFromParse()
        collectionView.backgroundColor = UIColor.clearColor()
        if UIScreen.mainScreen().bounds.size.height == 480.0 {
            let flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            flowLayout.itemSize = CGSizeMake(250.0, 300.0)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return trips.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CityCollectionCell
        
       
        let trip = trips[indexPath.row]
        cell.cityName.text = trip.city
        
        cell.cityImg.image = UIImage()
        if let featuredImage = trip.featuredImage {
            featuredImage.getDataInBackgroundWithBlock({ (imageData: NSData?, error: NSError?) -> Void in
                if let tripImageData = imageData {
                    cell.cityImg.image = UIImage(data:tripImageData)
                }
            })
        }
        cell.layer.cornerRadius = 2.0
        return cell
    }
    
    
    
    
    func loadTripsFromParse() {
   
        trips.removeAll(keepCapacity: true)
        collectionView.reloadData()
        
        let query = PFQuery(className:"attractions")
        query.cachePolicy = PFCachePolicy.NetworkElseCache
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            
            if let error = error {
                print("Error: \(error) \(error.userInfo)")
                return
            }
            
            if let objects = objects {
                for (index, object) in objects.enumerate() {
                    // Convert PFObject into Trip object
                    let trip = Attraction(pfObject: object)
                    self.trips.append(trip)
                    
                    let indexPath = NSIndexPath(forRow: index, inSection: 0)
                    self.collectionView.insertItemsAtIndexPaths([indexPath])
                }
            }
            
        }
    }
   
}